import NewTodo from './components/newTodo';
import Todos from './components/Todos';
import TodosContextProvider from './store/todos-context';

function App() {
  return (
    <TodosContextProvider>
      <NewTodo />
      <Todos />
    </TodosContextProvider>
  );
}

export default App;


// import { useState } from "react";
// import NewTodo from "./components/newTodo";
// import Todos from "./components/Todos";
// import Todo from "./models/todo";
// function App() {
//   // const todos = [
//   //   new Todo('Learn React'),
//   //   new Todo('Learn Typescript')
//   // ]
//   const [todos, setTodos] = useState<Todo[]>([])
//   const addTodoHandler =(todoText: string) => {
//     const newTodo = new Todo(todoText);

//     setTodos((prevTodos) => {
//       return prevTodos.concat(newTodo);
//     })
//   }
//   const removeTodoHandler = (todoId: string) => {
//       setTodos((prevTodos) => {
//         return prevTodos.filter(todo => todo.id !== todoId);
//       })
//   }
//   return (
//     <div className="App">
//       <NewTodo onAddTodo={addTodoHandler}/>
//       <Todos items={todos} onRemoveTodo={removeTodoHandler}/>
//     </div>
//   );
// }

// export default App;
